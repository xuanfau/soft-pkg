# SoftPkg

### 介绍

该库是用来存储相关的绿色软件以及脚本的。

### 已在软件及脚本

1. Everything

   一款用于搜索文件的工具，十分好用。

2. GifCam and ScreenToGif

   用于录制 Gif 图像的工具。

3. TrafficMonitor

   监测 CPU、GPU、网速、流量的工具。
